import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom";
import { getService } from '../api'

export default () => {
  const { id } = useParams()

  const [serviceDetail, setServiceDetail] = useState({
    id: null,
    attributes: {}
  })
  
  const back = () => {
    window.history.back()
  }

  useEffect(() => {
    getService(id).then(data => {
      setServiceDetail(data)
    })
  }, [])

  return (
    <>
      <h1>{serviceDetail.attributes.title}</h1>
      <div>{serviceDetail.attributes.description}</div>
      <a href="#" onClick={back}>&larr; Back</a>
    </>
  )
}