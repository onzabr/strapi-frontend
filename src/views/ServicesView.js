import React, {useState, useEffect} from 'react'
import { getServices } from '../api'

export default () => {
  const [servicesList, setServices] = useState([])

  useEffect(() => {
    getServices().then(data => {
      setServices(data)
    })
  }, [])

  return (
    <>
    <h1>Services</h1>
    {
      servicesList.map(service => (
        <div style={{marginBottom: '40px', borderBottom: '1px solid #d1d1d1'}}>
          <h3>{service.attributes.title}</h3>
          <div>{service.attributes.short_description}</div>
          <a href={`/detailed/${service.id}`}>Подробнее &rarr;</a>
        </div>
      ))
    }
    </>
  )
}