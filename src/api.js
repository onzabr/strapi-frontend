import axios from 'axios'
const http = axios.create({
  baseURL: 'http://localhost:1337/api/'
});


export function getServices() {
  return new Promise((resolve, reject) => {
    http.get('services', {
      params: {
        fields: ['title', 'short_description']
      }
    }).then(response => {
      resolve(response.data.data)
    }).catch(error => reject(error))
  })
}

export function getService(serviceId) {
  return new Promise((resolve, reject) => {
    http.get(`services/${serviceId}`).then(response => {
      resolve(response.data.data)
    }).catch(error => reject(error))
  })
}

export function postFeedback(feedback) {
  return new Promise((resolve, reject) => {
    http.post('feedbacks', { data: feedback }).then(response => {
      resolve(response.data.data)
    }).catch(error => reject(error))
  })
}