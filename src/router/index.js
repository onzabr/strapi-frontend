import { createBrowserRouter } from "react-router-dom"
import ServicesView from "../views/ServicesView"
import ServiceDetailedView from "../views/ServiceDetailedView"

export default createBrowserRouter([
  {
    path: "/",
    element: <ServicesView />,
  },
  {
    path: "/detailed/:id",
    element: <ServiceDetailedView />,
  },
]);